#include "linked_list.h"

/*!
-----------------------------------------------------
-------------------    CHANGE LOG   -----------------
-----------------------------------------------------
Date		Contributor		Details
----		-----------		-------
14-10-15  Ed        Moved struct typedefs back
12-10-15	Ed				Moved struct typedefs to source file
12-10-15	Ed				Initial revision
-----------------------------------------------------
*/



/* This function creates a new empty linked list header. Nodes must be added separately */
llhead * NewLinkedList(void)
{
  llhead *head;
  //Create the head of the list
  head = malloc(sizeof(llhead));
  
  //Populate the first node, with next pointing to itself.
  head->first = NULL;
  head->last = NULL;
  head->size = 0;
  
  //Return pointer to head node
  return head;
}

/* This function will insert data into the specified list after the specified entry (starts at 1) */
UINT ListInsert(llhead *head, UINT data, UINT idx_before)
{
  node *preceding_node_ptr = NULL;
  node *new_node_ptr = NULL;
  UINT idx = 1;
  
  if(IS_NULL(head))
  {
    return FALSE;
  }
  
  if(idx_before >= LINKED_LIST_SIZE_LIMIT)
  {
    return FALSE;
  }
  
  if(idx_before == head->size)
  {
    /* If we are being asked to insert an element after the last node, 
       we need to append instead of inserting */
    return ListAppend(head, data);
  }
  
  preceding_node_ptr = head->first;
  
  if(IS_NULL(preceding_node_ptr))
  {
    return FALSE;
  }
  
  do
  {
    if(idx == idx_before)
    {
      break;
    }
    //Point to next node
    preceding_node_ptr = preceding_node_ptr->next;
    idx++;
  }while(TRUE);
  
  new_node_ptr = malloc(sizeof(node));
  new_node_ptr->data = data;
  new_node_ptr->next = preceding_node_ptr->next;
  preceding_node_ptr->next = new_node_ptr;
  
  /* If we are in the situation where we need to insert a */
  if(preceding_node_ptr == head->last)
  {
    head->last = new_node_ptr;
  }
  
  if(ListCalcSize(head)== FALSE)
  {
    return FALSE;
  }
  
  return TRUE;
}

/* This function recalculates the size of a list, in the case of appending items */
UINT ListCalcSize(llhead *head)
{
  node *node_ptr = NULL;
  UINT count = 0;
  
  if(IS_NULL(head))
  {
    return FALSE;
  }
  
  node_ptr = head->first;
  
  if(IS_NULL(node_ptr))
  {
    return FALSE;
  }
    
  do
  {
    count++;
    node_ptr = node_ptr->next;
    if(node_ptr == head->first)
    {
      //We have looped back to start of list
      break;
    }
  }while(count < LINKED_LIST_SIZE_LIMIT);

  head->size = count;
  
  printf("Recalculated list size as %d\n", head->size);
  
  return TRUE;  
}

/* This function printfs the items in a list along with their addresses */
UINT ListEnumerate(llhead *head)
{
  node *node_ptr;
  UINT count = 0;
  if(IS_NULL(head))
  {
    return FALSE;
  }
  
  if(ListCalcSize(head) == FALSE)
  {
    return FALSE;
  }
  
  //Point at the top of the list
  node_ptr = head->first;
  //printf("First node at 0x%x, data %d, next at 0x%x\n", head->first, node_ptr->data, head->first->next);
  printf("\n|----- Enumerating list -----|\n");

  if(IS_NULL(node_ptr))
  {
    return FALSE;
  }
  
  do
  {
    printf("Element %d - ", count);
    printf("Addr 0x%x - ", node_ptr);
    printf("data %d - ", node_ptr->data);
    printf("next 0x%x\n", node_ptr->next);
    //Point to next node
    node_ptr = node_ptr->next;
    count++;
  }while (count < head->size);

  return TRUE;
}

/* Creates a node for the specified data and appends it to the list */
UINT ListAppend(llhead *head, UINT data)
{
  node *node_to_add;
  
  if(IS_NULL(head))
  {
    //Invalid list header!
    return FALSE;
  }
  
  if(head->size > LINKED_LIST_SIZE_LIMIT)
  {
    printf("Cannot append item, list will exceed max size of %d", LINKED_LIST_SIZE_LIMIT);
    return FALSE;
  }
  
  //Allocate memory for new node
  node_to_add = malloc(sizeof(node));
  
  if(IS_NULL(node_to_add))
  {
    //Could not allocate memory!
    return FALSE;
  }
  printf("\n|----- Appending to list -----|\n");

  //Set node data
  node_to_add->data = data;
  
  if( IS_NULL(head->first))
  {
    if( IS_NULL(head->last))
    {
      //List must be empty, add the first element
      //New node is the new first element
      head->first = node_to_add;
      head->last = node_to_add;
      node_to_add->next = node_to_add;
      printf("Adding first node at 0x%x, last at 0x%x\n", head->first, head->last);
      printf("Added %d to node at 0x%x, next at 0x%x\n", node_to_add->data, node_to_add, node_to_add->next);
    }
    else
    {
      //Something may have gone wrong, first is NULL but last is not!
      printf("Possible memory leak! first pointer is invalid but last is valid for list of size %d!\n", head->size);
      
      //Carry on and tidy up the list based on the information we have
      head->first = head->last;
      head->first->next = node_to_add;
      head->last = node_to_add;
      node_to_add->next = head->first;
    }
  }
  else
  {
    //Normal scenario, top and bottom of list is defined. Append node to end of list and move last
    head->last->next = node_to_add;
    head->last = node_to_add;
    //Make list circular by making next point to the other end of the list
    node_to_add->next = head->first;
    printf("Appended %d at 0x%x, next 0x%x\n", node_to_add->data, node_to_add, node_to_add->next);
  }
  
  if(ListCalcSize(head) == FALSE)
  {
    printf("Could not recalculate list size!\n");
    return FALSE;
  }
  
  return TRUE;
}

/* Appends a pre-existing node to the specified list */
UINT ListAppendNode(llhead *head, node *node_to_append)
{
  if(IS_NULL(head))
  {
    //Invalid list header!
    return FALSE;
  }
  
  if(IS_NULL(node_to_append))
  {
    //Invalid list header!
    return FALSE;
  }
  
  if(head->size == 0)
  {
    head->first = node_to_append;
    head->last = head->first;
  }
  else
  {
    head->last->next = node_to_append;
    head->last = node_to_append;
    head->last->next = head->first;
  }
  
  head->size++;
  
  return TRUE;

}

/* This function extracts a node from a list */
UINT ListExtract(llhead *head, node *node_to_extract)
{
  node *prev_node_ptr = NULL;
  
  prev_node_ptr = head->last;
    
  if(head->size == 1)
  {
    //Get rid of the head pointer since we are extracting the only node in the list
    head->first = NULL;
    head->last = NULL;
    head->size = 0;
    return TRUE;
  }
  
  while(prev_node_ptr->next != node_to_extract)
  {
    prev_node_ptr = prev_node_ptr->next;
    if(prev_node_ptr == head->last)
    {
      printf("Cannot find node in list!\n");
      return FALSE;
    }
  }


  prev_node_ptr->next = node_to_extract->next;
  
  if(node_to_extract == head->first)
  {
    head->first = head->first->next;
  }
  else if(node_to_extract == head->last)
  {
    head->last = prev_node_ptr;
  }
  
  node_to_extract->next = NULL;
  
  if(ListCalcSize(head) == FALSE)
  {
    printf("Could not recalculate list size!\n");
    return FALSE;
  }
  
  return TRUE;
}

/* This function will clean and free a list from the first element to the last. */
UINT FreeList(llhead *head)
{
  node *node_to_free;
  node *next_node;
  
  if(IS_NULL(head))
  {
    //Invalid head ptr
    return FALSE;
  }
  
  printf("\n|---------|\n");
  
  if(head->first != NULL)
  {
    do
    {
      //point at first element
      node_to_free = head->first;
      
      printf("Freeing node: data %d, list size %d\n", node_to_free->data, head->size);
      
      //Move the head->first to the next item (remove node_to_free from link chain)
      head->first = node_to_free->next;
      //Shrink list size
      head->size--;
      //Free the node
      free(node_to_free);
    }while(node_to_free != head->last);
  }
  //Free the list header - now that there is no data in the list
  printf("Freeing head ptr\n");
  free(head);
  return TRUE;
}

/* Swaps data of node with that of next node (will be slow for any non-trivial data type) */
void ListSwapDataWithNext(node *to_swap_forward)
{
  UINT data_temp = 0;
  
  data_temp = to_swap_forward->data;
  to_swap_forward->data = to_swap_forward->next->data;
  to_swap_forward->next->data = data_temp;
}
/* Swaps nodes via pointers (constant speed across data types) */
void ListSwapNodeWithNext(node *to_swap_forward, node *preceding_node)
{
  node *to_swap_backward = to_swap_forward->next;
  
  /* Need to jump the node to swap forward one link */
  to_swap_forward->next = to_swap_backward->next;
  /* Need to move the node to swap backward back one link */
  to_swap_backward->next = to_swap_forward;
  /* Need to preserve the continuity of the preceding nodes in the list */
  preceding_node->next = to_swap_backward;
}
/* This function finds the element within the list with the lowest value data */
node *ListFindMin(llhead *head)
{
  node *min_ptr = NULL;
  node *node_ptr = NULL;
  
  min_ptr = head->first;
  node_ptr = head->first;
  
  do
  {
    if(node_ptr->data < min_ptr->data)
    {
      min_ptr = node_ptr;
    }
    
    node_ptr = node_ptr->next;
  }while (node_ptr != head->first);

  return min_ptr;
}

/* This function finds the element within the list with the highest value data */
node *ListFindMax(llhead *head)
{
  node *max_ptr = NULL;
  node *node_ptr = NULL;
  
  max_ptr = head->first;
  node_ptr = head->first;
  
  do
  {
    if(node_ptr->data > max_ptr->data)
    {
      max_ptr = node_ptr;
    }
    
    node_ptr = node_ptr->next;
  }while (node_ptr != head->first);

  return max_ptr;
}

/* This function sorts a linked list by copying the elements to a new list until they are sorted. It then updates the list header */
UINT ListInsertionSort(llhead *head)
{
  node *min_ptr = NULL;
  llhead *temp_head;
  
  temp_head = NewLinkedList();
   
  do
  {
    printf("Finding min...\n");
    //Find the min
    min_ptr = ListFindMin(head);
    //Pop the min element off the list
    printf("Extracting min 0x%x: %d\n", min_ptr, ListExtract(head, min_ptr));
    /*Stick it at the end */
    printf("Appending to temp %d\n", ListAppendNode(temp_head, min_ptr));
    ListEnumerate(head);
  }while(head->size != 0);
  
  printf("Enumerate temp: %d\n", ListEnumerate(temp_head));
/*   head->first = temp_head.first;
  head->last = temp_head.last;
  head->size = temp_head.size; */
  *head = *temp_head;
  
  free(temp_head);
  
  return TRUE;

}

/* Used by Merge sort function */
UINT ListMergeAscending(llhead *left_head, llhead *right_head, llhead *result)
{
  node *node_ptr;
  
  while(left_head->size && right_head->size)
  {
    if(left_head->first->data <= right_head->first->data)
    {
      node_ptr = left_head->first;
      printf("Extracting node from left: %d\n", ListExtract(left_head, node_ptr));
      printf("Appending left to result: %d \n", ListAppendNode(result, node_ptr));
    }
    else
    {
      node_ptr = right_head->first;
      printf("Extracting node from right: %d\n", ListExtract(right_head, node_ptr));
      printf("Appending right to result: %d \n", ListAppendNode(result, node_ptr));
    }
  }
  
  while(left_head->size)
  {
    node_ptr = left_head->first;
    printf("Extracting node from left: %d\n", ListExtract(left_head, node_ptr));
    printf("Appending left to result: %d \n", ListAppendNode(result, node_ptr));
  }
  
  while(right_head->size)
  {
    node_ptr = right_head->first;
    printf("Extracting node from right: %d\n", ListExtract(right_head, node_ptr));
    printf("Appending right to result: %d \n", ListAppendNode(result, node_ptr));
  }

  return TRUE;
}

/* Merge sort implementation for linked list */
UINT ListMergeSort(llhead *head)
{
  llhead *left, *right;
  UINT mid_point = head->size/2;
  node *node_ptr = head->first;
  UINT i = 0;

  if(head->size <= 1)
  {
    return FALSE;
  }
  
  left = NewLinkedList();
  right = NewLinkedList();
  
  //printf("Mid point: %d\n", mid_point);
  
  do
  {
    //printf("Node ptr 0x%x\n", node_ptr);
    printf("Extracting node %d: %d\n", i, ListExtract(head, node_ptr));
    if(i < mid_point)
    {
      printf("Adding node %d to left list: %d\n", i, ListAppendNode(left, node_ptr));
    }
    else
    {
      printf("Adding node %d to right list: %d\n", i, ListAppendNode(right, node_ptr));
    }
    i++;
    node_ptr = head->first;
    
  //Loop will finish when we have extracted the last node from the head list
  }while(node_ptr != NULL);
  
  printf("Recursive sort left: %d\n", ListMergeSort(left));
  printf("Recursive sort right: %d\n", ListMergeSort(right));

  printf("Merging nodes: %d\n", ListMergeAscending(left, right, head));
  
  FreeList(left);
  FreeList(right);
  
  return TRUE;

}


/* DEPRECATE this in favour of merge sort? */
UINT ListSortAscending(llhead *head)
{
  node *node_ptr = NULL;
  node *prev_node_ptr = NULL;
  
  UINT min, max, no_swap_count = 0;
  UINT sort_done = FALSE;
  UINT total_loops = 0;
  
  if(IS_NULL(head))
  {
    return FALSE;
  }
  
  //Point at the top of the list
  node_ptr = head->first;
  prev_node_ptr = head->last;
  //printf("First node at 0x%x, data %d, next at 0x%x\n", head->first, node_ptr->data, head->first->next);
  printf("\n|----- Sorting list (Ascending) -----|\n");

  if(IS_NULL(node_ptr))
  {
    return FALSE;
  }
  
  min = node_ptr->data;
  max = min;
  
  do
  {
    //printf("Sort Done %d\n", sort_done);
    printf("Sorting...\n - Data: %d, Next: %d\n", node_ptr->data, node_ptr->next->data);
    if(node_ptr->next->data < node_ptr->data)
    {      
      printf("-Swapping...prev 0x%x, node 0x%x, next 0x%x\n", prev_node_ptr, node_ptr, node_ptr->next);
      ListSwapNodeWithNext(node_ptr, prev_node_ptr);
      
      ListEnumerate(head);
      
      if(prev_node_ptr->next == head->last)
      {
        //Special case, we have shifted the previous last node
        head->last = node_ptr;
        printf("Last node now contains %d\n", head->last->data);
      }
      else if(node_ptr->next == head->first)
      {
        //Special case, we have shifted the previous first node
        head->first = node_ptr;        
        printf("First node now contains %d\n", head->first->data);
      }
      //Traverse the previous node, current node has traversed for free because of the node swap
      prev_node_ptr = prev_node_ptr->next;
    }
    else
    {
      //We did not need to swap the nodes
      no_swap_count++;
      printf("no swap count: %d\n", no_swap_count);
      //Traverse to next node
      prev_node_ptr = node_ptr;
      node_ptr = node_ptr->next;
    }
    
    //Once we have gone through the whole list without swapping,
    // we know the list is sorted
    if(node_ptr == head->last)
    {
      // We need to increment the count and check against list size
      if(++no_swap_count >= head->size)
      {
        //Exit the loop
        sort_done = TRUE;
      }
      // Go back to beginning and traverse the list again.
      //no_swap_count = 0;
      node_ptr = head->first;
      no_swap_count = 0;
      //printf("----Back to start of list.\n");
    }
  }while(sort_done == FALSE);
  
  return TRUE;
}


