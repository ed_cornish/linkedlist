#include <malloc.h>
#include <stdlib.h>

/*
=================== Linked List ===================
- This implements a singly linked list using minimal libraries
- Only malloc and free are essential, other libraries are included
  for test and debug purposes
- This code implements Merge and Insertion sorts for the contents of
  a linked list
- The linked list nodes currently implement an unsigned int as a trivial
  data type for testing. Real world applications of this code would likely 
  use a structure of substantial size, neccessitating the modification of the 
  sort algorithm to allow for meaningful comparisons.
*/

/*!
-----------------------------------------------------
-------------------    CHANGE LOG   -----------------
-----------------------------------------------------
Date		Contributor		Details
----		-----------		-------
14-10-15  Ed        Moved struct typedefs back to this file
12-10-15	Ed				Moved prototypes and declarations to header, added comments
12-10-15	Ed				Initial revision
-----------------------------------------------------
*/
/* Using UINTs for everything */
#define FALSE 0
#define TRUE 1

//Explicit syntax for null checks
#define IS_NULL(p) ( p == NULL ? TRUE : FALSE)

//Arbitrary limit
#define LINKED_LIST_SIZE_LIMIT 1024

// Typedef for UINT shorthand
typedef unsigned int UINT;

// Forward typedef for node
typedef struct node node;
typedef struct llhead llhead;

//Structure to form each element of the linked list
struct node
{
  UINT data;  //Data for each element goes here (UINT for testing)
  node *next; //Pointer to next element in list
};

//Structure to manage linked list header with first, last and size info
struct llhead
{
  node *first;
  node *last;
  UINT size;
};
/* Function prototypes */
llhead * NewLinkedList(void);

/* This function will insert data as a new node into the specified list after the specified entry (starts at 1) */
UINT ListInsert(llhead *head, UINT data, UINT idx_before);
/* This function recalculates the size of a list, in the case of appending items */

UINT ListCalcSize(llhead *head);
/* This function printfs the items in a list along with their addresses */
UINT ListEnumerate(llhead *head);
/* Creates a node for the specified data and appends it to the list */
UINT ListAppend(llhead *head, UINT data);
/* Appends a pre-existing node to the specified list */
UINT ListAppendNode(llhead *head, node *node_to_append);
/* This function will clean and free a list from the first element to the last. */
UINT FreeList(llhead *head);
/* Swaps data of node with that of next node (will be slow for any non-trivial data type) */
void ListSwapDataWithNext(node *to_swap_forward);
/* Swaps nodes via pointers (constant speed across data types) */
void ListSwapNodeWithNext(node *to_swap_forward, node *preceding_node);
/* This function finds the element within the list with the lowest value data */
node *ListFindMin(llhead *head);
/* This function finds the element within the list with the highest value data */
node *ListFindMax(llhead *head);
/* This function sorts a linked list by copying the elements to a new list until they are sorted. It then updates the list header */
UINT ListInsertionSort(llhead *head);
/* Used by Merge sort function */
UINT ListMergeAscending(llhead *left_head, llhead *right_head, llhead *result);
/* Merge sort implementation for linked list */
UINT ListMergeSort(llhead *head);