#include <stdio.h>
#include "linked_list.h"

/* 	Main test function. The integer arguments supplied will be added to a linked list, 
	which will be enumerated, have 180 inserted, will be sorted, enumerated again, and 
	then sequentially freed */
int main ( int argc, char **argv )
{
  llhead *List1;
  UINT i;
  
  List1 = NewLinkedList();
  
  printf("Num Args: %d\n", argc);
  
  for(i = 1; i < argc; i++)
  {
    if(ListAppend(List1, strtoul(argv[i], NULL, 0)))
    {
      printf("Arg %d: %s\n", i, argv[i]);
    }
    else
    {
      printf("Append failed for arg %d (%s)!\n", i, argv[i]);
      return 1;
    }
  }

  
  printf("size of list is %d\n", List1->size);

  //Insert 180 halfway through the list
  ListInsert(List1, 180, List1->size/2);
  
  if(ListEnumerate(List1) == FALSE)
  {
    printf("Failed to enumerate list!\n");
    return 1;
  }
  
  printf("Min of list is %d\n", ListFindMin(List1)->data);
  printf("Max of list is %d\n", ListFindMax(List1)->data);
  
  //Sort the list ascending
  //ListInsertionSort(List1);
  printf("Merge Sort: %d\n", ListMergeSort(List1));
  
  if(ListEnumerate(List1) == FALSE)
  {
    printf("Failed to enumerate list!\n");
    return 1;
  }
  
  if(FreeList(List1) == FALSE)
  {
    printf("Failed to free list @ 0x%x\n", List1);
    return 1;
  }
  
  printf("Done with linked list program!\n");
  
  return 0;

}