# README #

### What is this repository for? ###

* This is an attempt to write a 'bare-bones' linked list implementation using C

### NOTES ###

* The malloc header is required for the implementation, stdlib for debugging
* The implementation includes a Merge sort and Insertion sort
* The intention behind this was to implement a linked list container functionality in the C 'style' (no OOP!), such as might be deployed on an embedded RTOS...